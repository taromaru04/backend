const express = require('express'); // Referencia al paquete express
const userFile = require('./user.json');
const bodyParser = require('body-parser');
const request = require('request-json')
var app = express();
app.use(bodyParser.json());
var port = process.env.PORT || 3000;
const URL_BASE = '/api-peru/v1/';

const baseMLabURL = 'https://api.mlab.com/api/1/databases/techu10db/collections/';
const apikeyMLab = 'NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF';

//solicitud a la api de MLab
app.get(URL_BASE + 'mongoUsers',
  function(req, res) {
    const httpClient = request.createClient(baseMLabURL);
    console.log("Cliente HTTP mLab creado.");
    const fieldParam = 'f={"_id":0}&';
   httpClient.get('user?' + fieldParam+'apiKey=' +apikeyMLab,
      function(err, respuestaMLab, body) {
        console.log('Error: ' + err);
        console.log('Respuesta MLab: ' + respuestaMLab);
        console.log('Body: ' + body);
        var response = {};
        if(err) {
            response = {"msg" : "Error al recuperar users de mLab."}
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {"msg" : "Usuario no encontrado."};
            res.status(404);
          }
        }
        res.send(response);
      }); //httpClient.get(
}); //app.get(


// Operacion GET todos los usuarios (Collections) (user,json)
app.get(URL_BASE + 'users',
    function(request,response){
        response.status(200);
        response.send(userFile);
});

// Operacion GET a varios usuarios con ID (Instance)
app.get(URL_BASE + 'users/:id/:id2/:id3',
    function(request,response){
      let indice = request.params.id;
      let indice2 = request.params.id2;
      console.log(indice);
      console.log(indice2);
      console.log(request.params.id3);
      response.status(200);
      let mensaje = "Muestra de usuarios";
      response.send(mensaje);

});

// Operacion GET a un usuario con ID (Instance)
app.get(URL_BASE + 'users/:id',
    function(request,response){
      let indice = request.params.id;
      let respuesta =
        (userFile[indice-1] == undefined)?{"msg":"No existe"}:userFile[indice-1];
      response.status(200).send(respuesta);
});

// PETICION GET con Query String
app.get(URL_BASE + 'usersq',
    function(request,response){
        //console.log('GET '+URL_BASE+' con query string');
        //console.log(request.query);
        let query = request.query;
        console.log(query.max);
        let arrayFinal = [];
        for(i=0;i<query.max;i++){
          arrayFinal.push(userFile[i]);
        }
        response.status(200);
        response.send(arrayFinal);
});

// Operacion POST a users
app.post(URL_BASE + 'users',
    function(req,res){
        // console.log(req.body);
        // console.log(req.body.id);
        var posicion = userFile.length;
        let newUser = {
          id:++posicion,
          first_name:req.body.first_name,
          last_name:req.body.last_name,
          email: req.body.email,
          password: req.body.password
        };
        userFile.push(newUser);
        res.status(201);
        res.send({"mensaje":"Usuario creado correctamente",
                 "usuario":newUser,
                 "userFile actualizado":userFile});
});

//Operacion PUT a users
app.put(URL_BASE +'users/:id',
    function (req, res) {
        let indice = req.params.id;
        let updateUser = {
          first_name:req.body.first_name,
          last_name:req.body.last_name,
          email: req.body.email,
          password: req.body.password
        };
        userFile[indice - 1].first_name = updateUser.first_name;
        userFile[indice - 1].last_name  = updateUser.last_name;
        userFile[indice - 1].email  = updateUser.email;
        userFile[indice - 1].password = updateUser.password;

        res.status(200);
        res.send({"mensaje":"Usuario actualizado correctamente",
                 "userFile actualizado":userFile});
});

//Operacion DELETE
app.delete(URL_BASE+'users/:id',
  function(request,response){
    let indice = request.params.id;
    let respuesta =
      (userFile[indice-1]==undefined)?
        {"msg":"No existe"} : {"Usuario Eliminado": (userFile.splice(indice-1,1))};
    response.status(202).send(respuesta);
});


// ejercicio para saber la longitud
app.get(URL_BASE + 'userlongitud',
        function(request,response){
            let myObject = {num_elem:userFile.length};
            let myJSON = JSON.stringify(myObject);
            response.status(200);
            response.send(myJSON);
});

// Servidor escuchara en la URL (servidor local)
// http://localhost:3000/holamundo
app.listen(port,function(){
    console.log('Node JS escuchando en el puerto 3000 ...')
});

// LOGIN - user.json
 app.post(URL_BASE + 'login',
  function(request, response) {
  // console.log("POST /apitechu/v1/login");
   console.log(request.body.email);
   console.log(request.body.password);
   var user = request.body.email;
   var pass = request.body.password;
   for(us of userFile) {
    if(us.email == user) {
     if(us.password == pass) {
      us.logged = true;
      writeUserDataToFile(userFile);
      console.log("Login correcto!");
      response.send({"msg" : "Login correcto.",
              "idUsuario" : us.id,
               "logged" : "true"});
     } else {
      console.log("Login incorrecto.");
      response.send({"msg" : "Login incorrecto."});
     }
    }
   }
 });
 // LOGOUT - users.json
 app.post(URL_BASE + 'logout',
  function(request, response) {
   //console.log("POST /apitechu/v1/logout");
   var userId = request.body.id;
   for(us of userFile) {
    if(us.id == userId) {
     if(us.logged) {
      delete us.logged; // borramos propiedad 'logged'
      writeUserDataToFile(userFile);
      console.log("Logout correcto!");
      response.send({"msg" : "Logout correcto.", "idUsuario" : us.id});
     } else {
      console.log("Logout incorrecto.");
      response.send({"msg" : "Logout incorrecto."});
     }
    }
   }
 });

 function writeUserDataToFile(data) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./user.json", jsonUserData, "utf8",
   function(err) { //función manejadora para gestionar errores de escritura
    if(err) {
     console.log(err);
    } else {
     console.log("Datos escritos en 'user.json'.");
    }
   })
 }
